const net = require('net');

let controllers = [];
let controllerStates = [];
let clients = [];

let server = net.createServer((socket) => {

  socket.on("data", (data) => {
    //console.log(data.toString());
    //console.log(data.toString().includes("10050"));
    //console.log(data);
    let close = true;
    // let close = false;

    // let msg = controllerState ? controllerState : "No controller registered!";
    if (clients.includes(socket)) {
      close = false;
      // socket.write(`${msg}\r\n`);
    }

    if (!clients.includes(socket) && data.toString().includes("10050")) {
      console.log("client connected");
      //console.log(clients.length);
      // close = false;
      clients.push(socket);
      // controllerStates.push("noState");
      console.log("clients no:", clients.length, "\n");
      socket.write(JSON.stringify(controllerStates) + "\n");
      // socket.destroy();
      // socket.write("Client registered\r\n");
      // socket.write(`${msg}\r\n`);
    }


    if (!controllers.includes(socket) && data.toString().includes("20050")) {
      console.log("arduino connected");
      //console.log(controllers.length);
      // close = false;  // check this line
      controllers.push(socket);
      controllerStates.push("noState");
      // controllerStates.push("CSTATE");
      console.log("arduinos no:", controllers.length, "\n");
      //socket.write("Controller registered!\r\n");
    } else if (controllers.includes(socket)) {
      controllerIdx = controllers.indexOf(socket);
      if (data.toString().includes("G1")) {
        //console.log(data.toString());
        //clients.forEach(client => {
	state = { id: controllerIdx, state: { green: true, yellow: false, red: false } };
	controllerStates[controllerIdx] = state;
	clients.forEach(client => {
          client.write(JSON.stringify( controllerStates ) + "\n");
        });
      }
      
      if (data.toString().includes("Y1")) {
	//console.log(data.toString());
        //clients.forEach(client => {
	state = { id: controllerIdx, state: { green: false, yellow: true, red: false } };
        controllerStates[controllerIdx] = state;
	clients.forEach(client => {
          client.write(JSON.stringify( controllerStates ) + "\n");
        });
      }

      if (data.toString().includes("R1")) {
	//console.log(data.toString());
        //clients.forEach(client => {
	state = { id: controllerIdx, state: { green: false, yellow: false, red: true } };
	controllerStates[controllerIdx] = state;
	clients.forEach(client => {
          client.write(JSON.stringify( controllerStates ) + "\n");
        });
      }
      
      if (data.toString().includes("Alarme")) {
	console.log(data.toString());
        clients.forEach(client => {
	  client.write("alarm\n");
	});
      }
    }
  
    if (controllers.includes(socket)) {
      close = false;
      // clients.forEach(clientSocket => clientSocket.write(`${controllerState}\r\n`));
    }

    if (clients.includes(socket)) {
      close = false;
    }

    if (close) {
      socket.destroy();
    }

  });

  socket.on("close", () => {
    clients = clients.filter((s) => s !== socket);
    controller = controllers.filter((s) => s !== socket);
    // controller = socket === controller ? null : controller;
    console.log("--\n", "Connection closed!", "--\n");
  });

});

server.listen(8082, '0.0.0.0');
