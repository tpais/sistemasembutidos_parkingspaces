# Parking Spaces
## Descrição do Projecto
3 Lugares de estacionamento, com um led cada que poderá ser verde, vermelho , amarelo.  2 sensores de distancia por lugar, 1 sensor de temperatura e 1 um de luz por lugar + uma campainha para o sistema de detecção de incêndio.

## Requisitos Funcionais
1. O Sistema deverá monitorizar se os lugares se encontram livres ou ocupados;
2. O Sistema deverá monitorizar a luz e a temperatura em cada lugar para a detecção de incêndio;
3. O Sistema deverá sinalizar com uma luz verde ou vermelha caso o lugar se encontre livre ou ocupado respectivamente;
4. O utilizador deverá ser capaz de visualizar o estado de ocupação dos lugares;
5. O utilizador deverá ser alertado no caso de detecção de incêndio;
6. O utilizador deverá ser capaz de reservar um lugar de forma antecipada, obrigando à activação da indicação de lugar ocupado.

## Planeamento tarefas (Gantt)

