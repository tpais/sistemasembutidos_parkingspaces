## Descrição do papel do android no sistema
Os sistemas android, no modelo em questão, irão servir essencialmente de frontend tendo como principal objectivo a simplificação da consulta do estado de ocupação/reserva do parque de estacionamento monitorizado e, no limite, serem capazes de comunicar uma nova reserva, para uma determinada matrícula, ao servidor central. Estes dispositivos irão, como tal, estar constantemente em comunicação com um servidor, para o caso será um dispositivo raspberry, sobre o qual irão executar inúmeras queries (ainda a definir se será SQL ou API) para que sejam capazes de demonstrar, na medida do possível, o estado actual do parque de estacionamento.

## Requisitos Funcionais Android
* Os dispositivos android, devem ser capazes de apresentar, na aplicação a desenvolver, o estado de ocupação do parque monitorizado. Para tal, deve ser apresentada uma interface visual que exemplifique a disposição dos lugares existentes no parque, numerando cada um deles, e associando ainda uma cor representativa do estado de ocupação (verde, amarelo ou vermelho)
[Ocupação](https://gitlab.com/tpais/sistemasembutidos_parkingspaces/raw/master/docs/Anima%C3%A7%C3%B5es/Entering.png)
[Desocupação](https://gitlab.com/tpais/sistemasembutidos_parkingspaces/raw/master/docs/Anima%C3%A7%C3%B5es/Leaving.png)
* Para além da visualização, deve ser possível efectuar reservas de um determinado lugar, desocupado, associando para isso uma matrícula. Esta reserva deve ser validada pelo servidor e caso seja exequível deve ser o estado da luz indicadora no lugar em causa para amarela. O mesmo terá de acontecer nas restantes interfaces móveis.
[Reserva](https://gitlab.com/tpais/sistemasembutidos_parkingspaces/raw/master/docs/Anima%C3%A7%C3%B5es/Reservation.png)
* Na interface gráfica deve também ser apresentada a temperatura actual do parque e, caso esta ultrapasse certos valores, deverá ser modificada a cor com que esta se encontra a ser apresentada.
* A interface deve notificar, de forma apelativa, os alertas notificados pelo servidor quando ultrapassados os limites máximos de temperatura, indicando a possibilidade de incêncido.
[Alarme de Incêndio](https://gitlab.com/tpais/sistemasembutidos_parkingspaces/raw/master/docs/Anima%C3%A7%C3%B5es/Fire.png)

## Requisitos Não Funcionais - Android
* O modelo a desenvolver deve suportar múltiplas conexões de interfaces gráficas, para o caso as aplicações android.
* Deve ser acautelado, na medida do possível, um mecanismo de sincronização entre interfaces aquando da ocorrência de uma nova reserva, executada por um outro dispositivo móvel.
* Devem também ser ponderados mecanismos de sincronização para a actualização dos dados apresentados em cada uma das interfaces, mesmo que não existam novas reservas e por isto entenda-se, ocupação ou desocupação de um lugar. A esta ideia está subjacente um tempo limite de 2 segundos para a execução deste processo.
* O mecanismo de alerta de incêndio deve também ele cumprir um requisito de 2 segundos, para notificar cada um dos dispositivos móveis conectado.
* Se possível, devem ser implementados mecanismos criptográficos para as comunicações realizadas entre estes dispositivos e o servidor raspberry.
* A interface deve ser simplista e intuitiva, orientando da melhor forma o utilizador no mapeamento do parque de estacionamento.

## Custos para o Projeto
* A componente de android, por si só, não irá acarretar qualquer custo uma vez que tudo poderá ser testado e demonstrado com recurso a máquinas virtuais.

################################################A Preencher no geral #########################################################

## Objetivo do Projeto

## Diagrama de Componentes de Hardware

## Diagrama de Colaboração entre Componentes

[Diagrama da Solução](https://gitlab.com/tpais/sistemasembutidos_parkingspaces/raw/master/docs/Anima%C3%A7%C3%B5es/Components.png)

## Diagrama de Gantt com tarefas e tempo

Datas Definidas:
13/03/2019 - Entrega do Planeamento
10/04/2019 - Ponto de situação
02/06/2019 - Apresentação

## Recursos

https://www.pubnub.com/blog/smart-parking-iot-simulation-with-realtime-space-monitoring/
