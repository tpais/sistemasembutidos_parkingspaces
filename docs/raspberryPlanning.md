## Descrição do papel do raspberry no sistema
O raspberry irá funcionar como elemento de interligação entre os restantes componentes do sistema, microcontrolador e telemóvel. Para isso o raspberry irá alojar um servidor escrito em NodeJS no qual se irão conectar os restantes componentes do sistema como clientes e que servirá para troca de mensagens de rede entre o arduino e o frontend (telemóvel android). O principal objectivo deste elemento central do sistema será partilhar o estado das medições efectudas pelos sensores instalados no arduino com a interface gráfica disponível no telemóvel android, e reencaminhar pedidos de reserva de lugar efectuados no frontend para o arduino, de forma a este sinalizar o lugar como ocupado.

## Requisitos
* O servidor deverá estar à escuta de mensagens enviadas pelo arduino com informações sobre mudanças de estado;
* O servidor terá que conseguir perceber quando a ligação ao arduino se encontrar indisponível;
* O servidor deverá ser capaz de enviar mensagens ao arduino de forma a forçar um estado (reserva de lugar);
* O servidor deverá ser capaz de enviar mensagens para o frontend forçando a atualização do estado;
* O servidor deverá ser capaz de receber mensagens vindas do frontend com pedidos de reserva de lugar;
* O estado trocado entre os dispositivos deverá conter informações sobre ocupação dos lugares (led vermelho ou verde ligados) e sobre o estado dos sensores de incêndio;
* Numa primeira fase de protótipo as mensagens trocadas seguirão um protocolo próprio sobre TCP a desenvolver com o sistema;
* Se tal for possível no tempo de desenvolvimento proposto, deverá ser introduzida uma layer de cifra das mensagens trocadas. Poderá ser estudada a solução do TLS;
* No caso de problemas no sistema de comunicações previsto, poderá ser adotada uma solução de ligação directa do raspberry ao arduino através de sinais digitais utilizando as interfaces disponíveis nos dois dispositivos.

![comms](https://gitlab.com/tpais/sistemasembutidos_parkingspaces/raw/master/docs/raspberryCommunications.png)