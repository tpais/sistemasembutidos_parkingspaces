
#include <WiFi.h>
#include <SPI.h>
#include <DHT.h>

#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
#define sensor_dist A3

DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino

int chk;
float hum;  //Stores humidity value
float temp; //Stores temperature value

// constants won't change. Used here to set a pin number:
const int greenLed =  5;
const int yellowLed = 6;
const int redLed = 8;
const int termometro= 2;
const int campainha=3;
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
//const int sensor_dist=A3;
int greenState = LOW;
int yellowState = LOW;
int redState = LOW;
int sensorValue=0;
bool flag=true;

char ssid[] = "ps-system";     //  your network SSID (name)
char pass[] = "f4f0c353-68fb-4d1c-b6ad-c9f114cf21cc"; 
int status = WL_IDLE_STATUS; 
WiFiClient client;




void setup() {
  // set the digital pin as output:
  pinMode(greenLed, OUTPUT);
  pinMode(yellowLed, OUTPUT);
  pinMode(redLed, OUTPUT);
  pinMode(campainha,OUTPUT);
 

 Serial.begin(9600);
 dht.begin();
 
  while (!Serial) {
  ; // wait for serial port to connect. Needed for native USB port only
  }
  
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network:
    status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:
    delay(2000);
  }

   Serial.print("You're connected to the network");
   client.connect("192.168.5.1",8082);
  
   client.println("20050"); 
   client.flush();

   
}

void loop() {
  float volts =analogRead(sensor_dist)*0.0048828125;
  int distance=13*pow(volts,-1);
 
  if(distance<10){
    // Serial.println(distance);
    }
   
  temp= dht.readTemperature();
 //Serial.println(temp);
  int a=1;
  if(temp>38 && temp<39 ){

         // digitalWrite(redLed, redState);
          
        
         
         tone(campainha,1000,100);
         delay(100);
         tone(campainha,800,100);
         delay(100);
         tone(campainha,600,100);
         delay(100);
         tone(campainha,400,100);
       
      }
   
  
  
  sensorValue = analogRead(analogInPin);
  
  if(sensorValue>=50 && distance<10){
    flag = true;
    if (redState == HIGH) {
      flag = false;
    }
    redState = HIGH;
    digitalWrite(redLed, redState);
    greenState = LOW;
    digitalWrite(greenLed, greenState);
    if(flag){
       client.flush();
      client.println("G0Y0R1");
      flag=false;
    }
    
   }
  
  if(sensorValue<50 || distance>10) {
    flag = true;
    if (greenState == HIGH) {
      flag = false;
    }
    redState = LOW;
    digitalWrite(redLed, redState);
    greenState = HIGH;
    digitalWrite(greenLed, greenState);
    if(flag){
       client.flush();
      client.println("G1Y0R0");
      flag=false;
    }
    
    
    }
  }
